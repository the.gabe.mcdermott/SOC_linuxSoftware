#include "controlSystem.h"
#include "commonIncludes.h"

#include "hps_0.h"
#include "led.h"
#include "seg7.h"
#include "switches.h"
#include "pushButtons.h"
#include "fibCore.h"
#include "pattern.h"
#include <stdio.h>


#define NUM_KEYS 4
static uint8_t previousKeyRead[NUM_KEYS]; 
static uint8_t state = 0;

void DisplayMathResults(uint8_t result, uint8_t opA, uint8_t opB);
uint8_t getMode(void);
uint8_t isNthBitSet(uint8_t data, uint8_t n);

void doFib(void);
void doMath(void);

void runControlSystemLoop(void)
{
	uint8_t readMode = getMode();
	
	//If the result is one of the valid states
	// (1-4) assign it.
	if( (readMode != 0) &&
	    (readMode <= 4))
	{
		//reset display
		SEG7_BlankAllModules(2);
		LEDR_AllOff();
		state = readMode;
	}
	switch(state)
	{
		case 0:
			printf("Idle state\n");
			break;
		case 1:
			printf("State 1\n");
			doMath();
			break;
		case 2:
			printf("State 2\n");
			doFib();
			break;
		case 3:
			printf("State 3\n");
			stepPatternCylon();
			break;
		case 4: 
			printf("State 4\n");
			stepPatternBars();	
			break;
		default:
			printf("default state\n");
			break;
	}
}

uint8_t getMode(void)
{
	uint8_t	currentKeyRead[4] = {0, 0, 0, 0};
	uint8_t mode = 0;
	uint8_t rawRead = 0;
	uint8_t i = 0;
	
	rawRead = readPushButtons();

	while(i < NUM_KEYS)
	{
		currentKeyRead[i] = isNthBitSet(rawRead, i);
		i++;	
	}
	i = 0;

	while(i < NUM_KEYS)
	{		
		if (previousKeyRead[i] != currentKeyRead[i])
		{
			if ((previousKeyRead[i] == 1) &&
			   (currentKeyRead[i] == 0))
			   {	
				mode = i + 1;
                           }	
			previousKeyRead[i] = currentKeyRead[i];
		}
		i++;
	}
	return mode;
}

void doFib(void)
{
	uint8_t sw_read = 0;
	sw_read = readSwitchBankA();

	uint8_t fibResult = 0;
	fibResult = getFibResult(sw_read);
	
	SEG7_DisplayHexOnNthModule(fibResult, 0);
}

void doCylon(void)
{
	stepPatternCylon();
}


void doMath(void)
{
	uint8_t operation = 0;
	operation = readSwitchBankC();
	
	uint8_t operand_A = 0;
	uint8_t operand_B = 0;
	operand_A = readSwitchBankA();
	operand_B = readSwitchBankB();
	

	uint8_t result = 0;
	switch (operation)
	{
		case 0:
			SEG7_BlankAllModules();
			break;
		case 1:
			result = operand_A + operand_B;
			DisplayMathResults(result, operand_A, operand_B);
			break;
		case 2:
			result = operand_A - operand_B;
			DisplayMathResults(result, operand_A, operand_B);
			break;
		case 3:
			result = operand_A * operand_B;
			DisplayMathResults(result, operand_A, operand_B);
			break;
		default:
			SEG7_BlankAllModules();
			break;	
	}
}

void DisplayMathResults(uint8_t result, uint8_t opA, uint8_t opB)
{
	SEG7_DisplayHexOnNthModule(result, 2);
	SEG7_DisplayHexOnNthModule(opB, 1);
	SEG7_DisplayHexOnNthModule(opA, 0);
}


uint8_t isNthBitSet(uint8_t data, uint8_t n)
{
	uint8_t result = 2;
	uint8_t	mask = 1;

	if ((n >= 0) &&
	   (n <= 7))
	{
		mask = mask << n;
		result = data & mask;
		result = result >> n;
	}

	printf("%dth bit is %d\n", n, result);
	return result;
}


uint8_t initControlSystem(void)
{
	uint8_t success = 0;
	initFibCore();
	
	uint8_t i = 0;
	while (i < NUM_KEYS)
	{
		previousKeyRead[i++] = 0; 
	}

	return success;	
}
