#ifndef CONTROL_SYSTEM
#define CONTROL_SYSTEM

#include "commonIncludes.h"
#include "hps_0.h"

	void runControlSystemLoop(void);
	uint8_t initControlSystem(void);
#endif //CONTROL_SYSTEM
