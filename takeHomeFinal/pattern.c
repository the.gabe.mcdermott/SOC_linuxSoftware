#include "pattern.h"
#include "commonIncludes.h"
#include "seg7.h"
#include "led.h"

const uint16_t  PATTERN_CYLON[18] = {0x003, 0x007, 0x00e, 0x01c,
				     0x038, 0x070, 0x0e0, 0x1c0,
				     0x380, 0x300, 0x380, 0x1c0,
				     0x0e0, 0x070,  0x038, 0x01c,
				     0x00e, 0x007};
const uint16_t PATTERN_CYLON_LEN = 18;

const uint16_t PATTERN_SPECIAL[10] = {0x30c, 0x198, 0x0F0,
	    			      0x060, 0x0F0, 0x198,
				      0x30c, 0x207, 0x003, 
				      0x207 };
const uint16_t PATTERN_SPECIAL_LEN = 10;
const uint16_t PATTERN_DISPLAY_BARS[12] = {0, 1, 0, 1, 4, 1, 4, 1, 4, 7, 4, 7};
const uint16_t PATTERN_BARS_LEN = 12;

static uint16_t patternCylonPosition = 0;
static uint16_t patternBarsPosition = 0;
static uint16_t patternBarsOffCount = 0;
static uint16_t patternSpecialPosition = 0;

void stepPatternCylon(void)
{
	LEDR_WriteData(PATTERN_CYLON[++patternCylonPosition%PATTERN_CYLON_LEN]);
}

void stepPatternBars(void)
{
	LEDR_WriteData(PATTERN_SPECIAL[++patternSpecialPosition%PATTERN_SPECIAL_LEN]);
	

	if (++patternBarsOffCount > 10)
	{
		uint8_t bar = 0;
		bar = PATTERN_DISPLAY_BARS[patternBarsPosition%PATTERN_BARS_LEN];
		patternBarsPosition++;
		SEG7_DisplayBarsToNthModule( bar, bar, 0);
		SEG7_DisplayBarsToNthModule( bar, bar, 1);
		SEG7_DisplayBarsToNthModule( bar, bar, 2);
		patternBarsOffCount = 0;
	}
}
