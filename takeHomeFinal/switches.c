#include "switches.h"
#include "commonIncludes.h"
#include "hps_0.h"
extern volatile unsigned long *h2p_lw_sw_addr;


uint16_t readSwitches(void)
{
	uint16_t returnThis = 0;
	returnThis = alt_read_word(h2p_lw_sw_addr);
	return returnThis;
}

uint8_t readSwitchBankA(void)
{
	return (readSwitches() & 0x000F );
}

uint8_t readSwitchBankB(void)
{
	return ((readSwitches() & 0x00F0 ) >> 4);
}

uint8_t readSwitchBankC(void)
{
	return ((readSwitches() & 0x0300 ) >> 8);
}
