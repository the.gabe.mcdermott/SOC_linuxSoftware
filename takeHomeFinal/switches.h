#ifndef SWITCHES_H
#define SWITCHES_H


#include "commonIncludes.h"
#include "hps_0.h"

uint16_t readSwitches(void);
uint8_t readSwitchBankA(void);
uint8_t readSwitchBankB(void);
uint8_t readSwitchBankC(void);
#endif //SWITCHES_H
