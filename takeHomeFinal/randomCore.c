#include "randomCore.h"

extern volatile unsigned long *h2p_lw_randomCore_addr;

uint32_t GetRandomNumber(void)
{
	//Garenteed random by fair dice roll
	uint32_t randomNum = 4;
	randomNum = alt_read_word(h2p_lw_randomCore_addr);
	return randomNum;
}
