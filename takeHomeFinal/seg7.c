#include <stdio.h>
#include <unistd.h>
#include "seg7.h"
#include "commonIncludes.h"

extern volatile unsigned long *h2p_lw_hex_01_addr;
extern volatile unsigned long *h2p_lw_hex_23_addr;
extern volatile unsigned long *h2p_lw_hex_45_addr;

#define SEG7_SET_01(seg_mask)   alt_write_word(h2p_lw_hex_01_addr, seg_mask)
#define SEG7_SET_23(seg_mask)   alt_write_word(h2p_lw_hex_23_addr, seg_mask)
#define SEG7_SET_45(seg_mask)   alt_write_word(h2p_lw_hex_45_addr, seg_mask)

static unsigned char oneDigitHexMap[] = {
        0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 
        0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E
    };  // 0,1,2,....9, a, b, c, d, e, f
 
static unsigned char oneDigitLineMap[] = {
	0xFF, //blank
	0xF7, //horizontal bottom only
	0xBF, //horizontal middle only
	0xFE, //horizontal top 	  only
	0xB7, //horizontal bottom AND middle
	0xF6, //horizontal bottom AND top
	0xBE, //horizontal middle AND top
	0xB6, //horizontal bottom AND middle AND top
	};

void SEG7_BlankNthModule(uint8_t n)
{
	unsigned long mask = 0xFFFF;
	SEG7_WriteMaskToNthModule(mask, n);
}

void SEG7_BlankAllModules()
{
	SEG7_BlankNthModule(0);
	SEG7_BlankNthModule(1);
	SEG7_BlankNthModule(2);
}

void SEG7_DisplayHexOnNthModule(uint8_t display_number, uint8_t n)
{
	unsigned long mask = 0;
	mask = SEG7_GenerateHexMask(display_number);
	SEG7_WriteMaskToNthModule(mask, n);
}



unsigned long SEG7_GenerateHexMask(uint8_t display_number)
{
	uint8_t left_4bit_value = 0;
	uint8_t right_4bit_value = 0;
	left_4bit_value = ((display_number & 0xF0) >> 4);
	right_4bit_value = (display_number & 0x0F);

	unsigned char left_mask = 0;
	unsigned char right_mask = 0;
	left_mask = oneDigitHexMap[left_4bit_value];
	right_mask = oneDigitHexMap[right_4bit_value];
	
	unsigned long returnMask = 0;
	returnMask = SEG7_PackIntoSingleMask(left_mask, right_mask);
	
	return returnMask;
}

void SEG7_DisplayBarsToNthModule(uint8_t barA, uint8_t barB, uint8_t n)
{
	unsigned long mask = 0;
	mask = SEG7_PackIntoSingleMask(oneDigitLineMap[barA], 
				       oneDigitLineMap[barB]);
	SEG7_WriteMaskToNthModule(mask, n);
}

void SEG7_DisplayHexPrefixAtNthModule(uint8_t n)
{
	unsigned long mask = 0;
	mask = SEG7_PackIntoSingleMask(oneDigitHexMap[0], 0x09);
	SEG7_WriteMaskToNthModule(mask, n);
}	

unsigned long SEG7_PackIntoSingleMask(unsigned char dig_lhs, unsigned char dig_rhs)
{
	unsigned long  writeThis = 0;
	unsigned long mask = 0x3F80;
	unsigned long temp_lhs = (dig_lhs << 7) & mask;

	unsigned long mask_rhs = 0x007F;
	unsigned long temp_rhs = dig_rhs & mask_rhs;

	writeThis = temp_lhs | temp_rhs;
	return writeThis;
}

void SEG7_WriteMaskToNthModule(unsigned long maskToWrite, uint8_t n)
{
	switch(n)
	{
		case 0: SEG7_SET_01(maskToWrite); break;
		case 1: SEG7_SET_23(maskToWrite); break;
		case 2: SEG7_SET_45(maskToWrite); break;
	}
}
