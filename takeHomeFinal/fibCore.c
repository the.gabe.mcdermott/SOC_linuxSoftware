#include "fibCore.h"
#include "commonIncludes.h"
#include <stdio.h>
#include <unistd.h>

#define FIB_REG_WRITE(index, data)	alt_write_word(h2p_lw_fibCore_addr+index, data)
#define FIB_REG_READ(index)		alt_read_word(h2p_lw_fibCore_addr+index)

extern volatile unsigned long *h2p_lw_fibCore_addr;
enum addr_reg { READY = 0, 
		FIB_COUNT = 1,
		START_CALC = 2,
		RESULT = 3};

void initFibCore(void)
{
	enum addr_reg addrVal = FIB_COUNT;
	const int SOME_INIT_VAL = 0;
	//Send init signal
	FIB_REG_WRITE(addrVal, SOME_INIT_VAL);
}


uint8_t getFibResult(uint8_t countTo)
{
	uint8_t returnThis = 0;
	uint8_t calc_done = 0;
	const uint8_t START_CALC_GO = 1;
	const uint8_t START_CALC_STOP = 0;

	const uint16_t READY_ADDR = 0, 
		       FIB_COUNT_ADDR = 1, 
		       START_CALC_ADDR = 2,
                       RESULT_ADDR = 3;

	//If params are good
	//And the circuit is ready
	if (countTo >= 0 &&
            countTo <= 15)
	{	
		//Unset start bit to intilize
		FIB_REG_WRITE(START_CALC, START_CALC_STOP);		

		// If the system is ready
		if (FIB_REG_READ(READY_ADDR) == 1)
		{	
			//assign count
			FIB_REG_WRITE(FIB_COUNT_ADDR, countTo);
			
			//set start bit
			FIB_REG_WRITE(START_CALC_ADDR, START_CALC_GO);
			
			//Unset start bit
			FIB_REG_WRITE(START_CALC, START_CALC_STOP);		

			//Wait for ready bit
			while (calc_done != 1)
			{	
				//Read the results
				calc_done = FIB_REG_READ(READY_ADDR);
				usleep(1);		
			}

			//read result
			returnThis = FIB_REG_READ(RESULT_ADDR);
		}
	}
	return returnThis;
}


uint8_t fibPrintAllRegs()
{
	uint8_t printThis = 0;
	printf("////////////FibRegDump_START///////\n");
	printThis = FIB_REG_READ(READY);
	printf("Ready Reg %d\n", printThis);

	printThis = FIB_REG_READ(FIB_COUNT);
	printf("FIB_COUNT_REG Reg %d\n", printThis);
	
	printThis = FIB_REG_READ(START_CALC);
	printf("START_CALC Reg %d\n", printThis);

	printThis = FIB_REG_READ(RESULT);
	printf("RESULT Reg %d\n", printThis);

	printf("//////////FibRegDump_END///////////\n");
	return 0;
}
