#ifndef FIB_CORE
#define FIB_CORE
#include "commonIncludes.h"

void initFibCore(void);
uint8_t getFibResult(uint8_t countTo);
uint8_t fibPrintAllRegs();

#endif //FIB_CORE
