#ifndef SEG7_H_
#define SEG7_H_

#include "commonIncludes.h"

void SEG7_DisplayHexOnNthModule(uint8_t display_number, uint8_t n);
void SEG7_DisplayHexPrefixAtNthModule(uint8_t n);
void SEG7_DisplayBarsToNthModule(uint8_t barA, uint8_t barB, uint8_t n);
void SEG7_BlankNthModule(uint8_t n);
void SEG7_BlankAllModules();
unsigned long SEG7_GenerateHexMask(uint8_t display_number);
unsigned long SEG7_PackIntoSingleMask(unsigned char dig_lhs, unsigned char dig_rhs);
void SEG7_WriteMaskToNthModule(unsigned long maskToWrite, uint8_t n);

#endif /*SEG7_H_*/
