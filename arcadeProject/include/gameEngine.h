#ifndef GAME_ENGINE
#define GAME_ENGINE
#include "feild.h"
#include "player.h"
#include "collisionController.h"
#include "beaconController.h"

typedef struct GameEngine {
    int m_numPlays;
    int m_score;
    int m_difficulty;
    Feild m_feild;
    Player m_player;
    CollisionController m_collisionController;
    BeaconController m_beaconController;
} GameEngine;


int InitGame(GameEngine * game);
int CleanupGame(GameEngine * game);
int RunGameLoop(GameEngine * game, int difficulty);

#endif 

