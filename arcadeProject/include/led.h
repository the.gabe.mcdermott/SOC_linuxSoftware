#ifndef LED_H_
#define LED_H_

#include "commonIncludes.h"

void LEDR_LightCount(unsigned char LightCount);
void LEDR_OffCount(unsigned char OffCount);
void LEDR_AllOn(void);
void LEDR_AllOff(void);
void LEDR_WriteData(uint16_t data);

#endif /*LED_H_*/
