#ifndef COLLISION_CONTROLLER
#define COLLISION_CONTROLLER

#include "feild.h"
#include "player.h"
#include "position.h"

typedef struct CollisionController {
    int m_numCollisions;
    int m_x_diff;
    int m_y_diff;
} CollisionController;

//Initilize The CollisionController with all the objects it should track
//for collisions.
//Returns 0 unless an error occured.
int InitCollisionController(CollisionController * controller);

//Resets CollisionController to default values, removes
//stored references to objects tracked for collision.
int CleanupCollisionController(CollisionController * controller);

//Draw difference between player and beacon
//In the top left hand corner of the feild
int DrawCollisionController(CollisionController * controller);


//Checks if the given position lies within the given feild.
//Returns 0 if within the feild, else returns 1
int CheckCollisionFeild(CollisionController * controller,
                            Feild * feild,
                            Position * checkPosition);

//Checks if positionA and PositionB are colliding with each other.
//Returns 0 if no collision. Returns 1 if collision.
int CheckCollisionTwoPositions(CollisionController * controller,
                                Position positionA,
                                Position positionB);


#endif
