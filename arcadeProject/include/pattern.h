#ifndef PATTERN
#define PATTERN
#include <stdio.h>

void stepPatternCylon(void);
//Tahnks 0, 1, and 2 are valid.:
//Set maxFuel to maximum allowed fuel level.
//Will scale the display to show fuelLevel as
//A fraction of maxFuelLevel
void writeFuelDisplayNthTank(int fuelLevel,
                             int n,
                             int maxFuel);
void stepPatternBars(void);
#endif //PATTERN
