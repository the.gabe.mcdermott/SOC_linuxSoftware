#ifndef PLAYER
#define PLAYER
#include "position.h"
#include "pattern.h"

typedef struct Player {
    Position m_pos;
    Position m_speed;
    int m_life;
    int m_fuelTankLeft;
    int m_fuelTankMid;
    int m_fuelTankRight;
    int m_engineStates[4];
} Player;

//Initilize Player vars
//Returns 0 unless an error occured.
int InitPlayer(Player * player);

//Collets input, moves player, drops bombs,
//And checks for collisions
//Returns 0 unless an error occured.
int DoPlayerLogic(Player * player);

//Draws player on screen,
//Draws player fuel bars on 7 seg display
//Draws player ammo on LEDs
//Returns 0 unless an error occured.
int DrawPlayer(Player * player);

//resets Player vars to default
//Returns 0 unless an error occured.
int CleanupPlayer(Player * player);

//Returns 1 if life gone,
//else returns 0. 
int CheckLife(Player * player);

//Returns a pointer to the player mask
Position GetPlayerPosition(Player * player);

//Adds specified amount to player fuel tanks
//Tanks max out at starting value
int PlayerAddFuelToTanks(Player * player, int addFuelAmmount);


#endif
