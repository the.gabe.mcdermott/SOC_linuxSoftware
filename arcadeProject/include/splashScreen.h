#ifndef SPLASH_SCREEN
#define SPLASH_SCREEN

#include <stdlib.h>
#include <ncurses.h>
#include "pushButtons.h"
#include "led.h"
#include "switches.h"
#include "feild.h"

typedef struct SplashScreen {
    int m_highscore;
    Feild m_feild;
} SplashScreen;

int InitSplashScreen(SplashScreen * screen);
int CleanupSplashScreen(SplashScreen * screen);
int RunSplashScreen(SplashScreen * screen, int oldScore);
#endif
