#ifndef BEACON_CONTROLLER
#define BEACON_CONTROLLER

#include "feild.h"
#include "position.h"
#include "randomCore.h"
//Include random.h

typedef struct BeaconController {
    Position m_activeBeacon;
    int m_beaconsRemaining;
    Feild m_feild;
} BeaconController;

int InitBeaconController(BeaconController * controller, Feild * feild);
int CleanupBeaconController(BeaconController * beaconController);
int DrawBeaconController(BeaconController * beaconController);
Position GetActiveBeaconPosition(BeaconController * beaconController);

//Returns the number of beacons remaining.
int PlaceNextBeaconInFeild(BeaconController * controller,
                           Feild * feild);

#endif //BEACON_CONTROLLER_H

