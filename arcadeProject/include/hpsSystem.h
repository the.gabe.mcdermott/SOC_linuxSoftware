#ifndef HPS_SYSTEM
#define HPS_SYSTEM

volatile unsigned long *h2p_lw_randomCore_addr;
volatile unsigned long *h2p_lw_button_addr;
volatile unsigned long *h2p_lw_led_addr;
volatile unsigned long *h2p_lw_hex_01_addr;
volatile unsigned long *h2p_lw_hex_23_addr;
volatile unsigned long *h2p_lw_hex_45_addr;
volatile unsigned long *h2p_lw_sw_addr;

int mapAddresses(void** virtual_base, int * fd);
int unmapAddresses(void** virtual_base, int*  fd);
#endif
