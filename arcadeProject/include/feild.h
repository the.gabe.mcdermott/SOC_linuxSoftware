#ifndef FEILD
#define FEILD

typedef struct Feild {
    int m_origin_x;
    int m_origin_y;
    int m_max_x;
    int m_max_y;
} Feild;

int InitFeild(Feild * feild);
int CleanupFeild(Feild * feild);
int DrawFeildBorder(Feild * feild);
#endif
