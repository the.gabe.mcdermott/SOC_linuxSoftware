#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/mman.h>
#include <stdbool.h>

#include "../include/commonIncludes.h"
#include "../include/hpsSystem.h"
#include "../include/hps_0.h"


#define HW_REGS_BASE ( ALT_STM_OFST)
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

extern volatile unsigned long *h2p_lw_randomCore_addr;
extern volatile unsigned long *h2p_lw_button_addr;
extern volatile unsigned long *h2p_lw_led_addr;
extern volatile unsigned long *h2p_lw_hex_01_addr;
extern volatile unsigned long *h2p_lw_hex_23_addr;
extern volatile unsigned long *h2p_lw_hex_45_addr;
extern volatile unsigned long *h2p_lw_sw_addr;

int mapAddresses(void** virtual_base, int * fd)
{
	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
	if( ( *fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}
	*virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, *fd, HW_REGS_BASE );	
	if( *virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( *fd );
		return(1);
	}
	
	h2p_lw_led_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + PIO_LED_BASE) & ( unsigned long)( HW_REGS_MASK ) );
	
	h2p_lw_hex_01_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + PIO_SEVENSEG_01_BASE) & ( unsigned long)( HW_REGS_MASK ) );
	
	h2p_lw_hex_23_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + PIO_SEVENSEG_23_BASE) & ( unsigned long)( HW_REGS_MASK ) );
	
	h2p_lw_hex_45_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + PIO_SEVENSEG_45_BASE) & ( unsigned long)( HW_REGS_MASK ) );

	h2p_lw_sw_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + SWITCHES_BASE) & ( unsigned long)( HW_REGS_MASK ) );

	h2p_lw_button_addr=*virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + PIO_PUSHBUTTONS_BASE) & ( unsigned long)( HW_REGS_MASK ) );
	
    h2p_lw_randomCore_addr=*virtual_base + ( ( unsigned long ) ( ALT_LWFPGASLVS_OFST + RANDOMGEN_0_BASE) & (unsigned long)(  HW_REGS_MASK ) );
    return 0;
}

int unmapAddresses(void** virtual_base, int * fd)
{
    if( munmap( *virtual_base, HW_REGS_SPAN ) != 0 ) {
		printf( "ERROR: munmap() failed...\n" );
		close( * fd );
		return( 1 );

	}
	close( * fd );
    return( 0 );
}



