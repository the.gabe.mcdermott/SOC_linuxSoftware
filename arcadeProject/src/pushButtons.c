#include "../include/commonIncludes.h"
#include "../include/pushButtons.h"


extern volatile unsigned long *h2p_lw_button_addr;

int readPushButtons()
{
	int returnThis = 0;
	returnThis = alt_read_word(h2p_lw_button_addr);
	return returnThis;
}
