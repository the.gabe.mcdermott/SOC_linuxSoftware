#include "../include/player.h"
#include "../include/pushButtons.h"
#include "../include/led.h"
#include <ncurses.h>


int UpdatePosition(Player * player);
int AddGravity(Player * player);
int GetInput(Player * player);
int DrawFuel(Player * player);
int DrawPlayerPosition(Player * player);
int DrawEngineSprites(Player * player);

char const PLAYER_SPRITE[6] = "\\0*0/";
int const PLAYER_SPRITE_X_OFFSET = -2;
int const PLAYER_SPRITE_Y_OFFSET = 0;
char const PLAYER_ENGINE_SPRITE[1] = "o";
int const STARTING_FUEL = 900;
int const FUEL_COST = 10;
int const PLAYER_START_LIFE = 1;
int const PLAYER_NUM_ENGINES = 4;
int const PLAYER_ENGINE_OFF_STATE = 0;
int const PLAYER_ENGINE_ON_STATE = 1;
int const PLAYER_ENGINE_START_STATE = 0;

int InitPlayer(Player * player)
{
    player->m_pos.m_x = 46;
    player->m_pos.m_y = 1;
    player->m_speed.m_x = 0;
    player->m_speed.m_y = 1;
    
    player->m_life = PLAYER_START_LIFE;

    player->m_fuelTankLeft = STARTING_FUEL;
    player->m_fuelTankMid = STARTING_FUEL;
    player->m_fuelTankRight = STARTING_FUEL;
    int i = 0;
    for (i = 0; i < PLAYER_NUM_ENGINES; i++)
    {
        player->m_engineStates[i] = PLAYER_ENGINE_START_STATE;
    }
    return 0;
}

int DoPlayerLogic(Player * player)
{
    GetInput(player);
    UpdatePosition(player);
    AddGravity(player);
    return 0;
}


int DrawPlayer(Player * player)
{
    //Draw player sprite
    mvprintw(player->m_pos.m_y + PLAYER_SPRITE_Y_OFFSET, 
             player->m_pos.m_x + PLAYER_SPRITE_X_OFFSET, 
             PLAYER_SPRITE);
   
    //Draw star at players heart
    //Change color?
    mvprintw(player->m_pos.m_y,
             player->m_pos.m_x,
             "*");
    
    DrawPlayerPosition(player);
    DrawEngineSprites(player);        
    DrawFuel(player);

    return 0;
}

int CleanupPlayer(Player * player)
{
    return InitPlayer(player);
}

int CheckLife(Player * player)
{
    if(player->m_life == 0)
        return 1;
    else
        return 0;
}

Position GetPlayerPosition(Player * player)
{
    Position returnThis;
    returnThis.m_x = player->m_pos.m_x;
    returnThis.m_y = player->m_pos.m_y;

    return returnThis;
}

int PlayerAddFuelToTanks(Player * player, int fuelAmount)
{
    player->m_fuelTankLeft  += fuelAmount;
    if (player->m_fuelTankLeft > STARTING_FUEL)
        player->m_fuelTankLeft = STARTING_FUEL;

    player->m_fuelTankMid   += fuelAmount; 
    if (player->m_fuelTankMid > STARTING_FUEL)
        player->m_fuelTankMid = STARTING_FUEL;

    player->m_fuelTankRight += fuelAmount;
    if (player->m_fuelTankRight > STARTING_FUEL)
        player->m_fuelTankRight = STARTING_FUEL;

    return 0;
}

/////////   \/ Private Functions \/ ////////////////////

int UpdatePosition(Player * player)
{
    player->m_pos.m_x += player->m_speed.m_x;
    player->m_pos.m_y += player->m_speed.m_y;
    return 0;
}


int GetInput(Player * player)
{
    int rawRead = 0;
    rawRead = readPushButtons();
    
    int adjust_x = 0;
    int adjust_y = 0;    

    //If far left pressed
    if(~rawRead & 0x8)
    { 
        if (player->m_fuelTankLeft > 0)
        {
            adjust_x += 1;
            player->m_fuelTankLeft -= FUEL_COST;
            player->m_engineStates[0] = PLAYER_ENGINE_ON_STATE;
        }
    }
    else
        player->m_engineStates[0] = PLAYER_ENGINE_OFF_STATE;

    // if mid left pressed
    if(~rawRead & 0x4)  
    {
        if (player->m_fuelTankMid > 0)
        {
            adjust_x += 1;
            adjust_y += -1;

            player->m_fuelTankMid -= FUEL_COST;
            player->m_engineStates[1] = PLAYER_ENGINE_ON_STATE;
        }
    }
    else
        player->m_engineStates[1] = PLAYER_ENGINE_OFF_STATE;


    // if mid right pressed
    if(~rawRead & 0x2)
    {
        if (player->m_fuelTankMid > 0)
        {
            adjust_x += -1;
            adjust_y += -1;
        
            player->m_fuelTankMid -= FUEL_COST;
            player->m_engineStates[2] = PLAYER_ENGINE_ON_STATE;
        }
    }
    else
        player->m_engineStates[2] = PLAYER_ENGINE_OFF_STATE;

    // if far right pressed
    if(~rawRead & 0x1)
    {
        if (player->m_fuelTankRight > 0)
        {
            adjust_x += -1;

            player->m_fuelTankRight -= FUEL_COST;
            player->m_engineStates[3] = PLAYER_ENGINE_ON_STATE;
        }
    }
    else
        player->m_engineStates[3] = PLAYER_ENGINE_OFF_STATE;

    player->m_speed.m_x += adjust_x;
    player->m_speed.m_y += adjust_y;
    return 0;
}


int AddGravity(Player * player)
{
    //Effects of gravity
    if(player->m_speed.m_y < 1)
        player->m_speed.m_y++;

    return 0;
}

int DrawEngineSprites(Player * player)
{ 
    if (player->m_engineStates[0] == PLAYER_ENGINE_ON_STATE)
    {
        mvprintw(player->m_pos.m_y,
                 player->m_pos.m_x - 3,
                 PLAYER_ENGINE_SPRITE);
    }

    if (player->m_engineStates[1] == PLAYER_ENGINE_ON_STATE)
    {
        mvprintw(player->m_pos.m_y + 1,
                 player->m_pos.m_x - 2,
                 PLAYER_ENGINE_SPRITE);
    }  
    
    if (player->m_engineStates[2] == PLAYER_ENGINE_ON_STATE)
    {
        mvprintw(player->m_pos.m_y + 1,
                 player->m_pos.m_x + 2,
                 PLAYER_ENGINE_SPRITE);
    }
    
    if (player->m_engineStates[3] == PLAYER_ENGINE_ON_STATE)
    {
        mvprintw(player->m_pos.m_y,
                 player->m_pos.m_x + 3,
                 PLAYER_ENGINE_SPRITE);
    }
    return 0;
}
int DrawFuel(Player * player)
{
    
    writeFuelDisplayNthTank(player->m_fuelTankLeft,
                            2,
                            STARTING_FUEL);
    writeFuelDisplayNthTank(player->m_fuelTankMid,
                            1,
                            STARTING_FUEL);
    writeFuelDisplayNthTank(player->m_fuelTankRight, 
                            0,
                            STARTING_FUEL);
    
    if ((player->m_engineStates[0] == PLAYER_ENGINE_ON_STATE) ||
        (player->m_engineStates[1] == PLAYER_ENGINE_ON_STATE) ||
        (player->m_engineStates[2] == PLAYER_ENGINE_ON_STATE) ||
        (player->m_engineStates[3] == PLAYER_ENGINE_ON_STATE))
    {
        stepPatternCylon();
    }

    /*
    mvprintw(player->m_pos.m_y-5, 
             player->m_pos.m_x-3, 
             "%d",player->m_fuelTankLeft);

    mvprintw(player->m_pos.m_y-4, 
             player->m_pos.m_x-3, 
             "%d", player->m_fuelTankMid);
    
    mvprintw(player->m_pos.m_y-3, 
             player->m_pos.m_x-3, 
             "%d", player->m_fuelTankRight);
    */
    return 1;
}

int DrawPlayerPosition(Player * player)
{
    const int DRAW_POS_AT_Y = 2;
    const int DRAW_POS_AT_X = 2;

    mvprintw(DRAW_POS_AT_Y, 
             DRAW_POS_AT_X, 
             "%d",player->m_pos.m_x);
    mvprintw(DRAW_POS_AT_Y,
             DRAW_POS_AT_X + 3,
             ",");
    mvprintw(DRAW_POS_AT_Y,
             DRAW_POS_AT_X + 6,
             "%d",player->m_pos.m_y);
    return 0;
}
