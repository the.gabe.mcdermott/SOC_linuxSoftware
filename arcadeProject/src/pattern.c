#include "../include/pattern.h"
#include "../include/commonIncludes.h"
#include "../include/seg7.h"
#include "../include/led.h"

const uint16_t  PATTERN_CYLON[18] = {0x003, 0x007, 0x00e, 0x01c,
				     0x038, 0x070, 0x0e0, 0x1c0,
				     0x380, 0x300, 0x380, 0x1c0,
				     0x0e0, 0x070,  0x038, 0x01c,
				     0x00e, 0x007};
const uint16_t PATTERN_CYLON_LEN = 18;

const uint16_t PATTERN_SPECIAL[10] = {0x30c, 0x198, 0x0F0,
	    			      0x060, 0x0F0, 0x198,
				      0x30c, 0x207, 0x003, 
				      0x207 };
const uint16_t PATTERN_SPECIAL_LEN = 10;
const uint16_t PATTERN_DISPLAY_BARS[4] = {0, 1, 4, 7};
const uint16_t PATTERN_BARS_LEN = 4;
const uint16_t PATTERN_BARS_LEN_DOUBLE = 8;
#define PATTERN_DISPLAY_BARS_FULL_TANK  PATTERN_DISPLAY_BARS[PATTERN_BARS_LEN - 1] 
#define PATTERN_DISPLAY_BARS_EMPTY_TANK PATTERN_DISPLAY_BARS[0]
static uint16_t patternCylonPosition = 0;
static uint16_t patternBarsPosition = 0;
static uint16_t patternBarsOffCount = 0;
static uint16_t patternSpecialPosition = 0;



void stepPatternCylon(void)
{
	LEDR_WriteData(PATTERN_CYLON[++patternCylonPosition%PATTERN_CYLON_LEN]);
}


void writeFuelDisplayNthTank(int fuelLevel, 
                             int n, 
                             int maxFuel)
{
    
    uint8_t barLeftDigit = 0;
    uint8_t barRightDigit = 0;

    //Boundary checking for tank selection
    if ((n < 0) || (n > 2))
        n = 0;
    
    
   
    //Determine How many bars to display on right and left digits of the
    //7-seg display module.
    if(fuelLevel == maxFuel)
    {
        //If fuel is at the max level, simply display the maximum fuel amount.
        //For both digits.
        barLeftDigit = PATTERN_DISPLAY_BARS_FULL_TANK;
        barRightDigit = PATTERN_DISPLAY_BARS_FULL_TANK;   
    }
    else if(fuelLevel == 0)
    {
        //If the fule is at the min level, display no bars for both digits
        barLeftDigit = PATTERN_DISPLAY_BARS_EMPTY_TANK;
        barRightDigit = PATTERN_DISPLAY_BARS_EMPTY_TANK;   
    }
    else //Someplace in-between those two extremes
    { 
        //Scale the fuelLevel down to a value between the total number of bars 
        //we can display and zero. Basicly, fit the fuel value into the range
        //our display can show. 
        int scaledFuelLevel = (fuelLevel*(PATTERN_BARS_LEN_DOUBLE))/maxFuel;
        
        if (scaledFuelLevel >= PATTERN_BARS_LEN)
        {
            //If fuel is at or above above halfway, 
            //scale down the left hand digit, 
            //And draw a full mark on the right hand digit.
		    barLeftDigit = PATTERN_DISPLAY_BARS[scaledFuelLevel-PATTERN_BARS_LEN];
            barRightDigit = PATTERN_DISPLAY_BARS_FULL_TANK;
        }
        else
        {
            //If fuel level below halfway draw a zero on the left hand digit  
            //and scale down the right hand digit
		    barLeftDigit = PATTERN_DISPLAY_BARS_EMPTY_TANK;
            barRightDigit = PATTERN_DISPLAY_BARS[scaledFuelLevel];
        }
    }
    
    //Write the values found to the 7-seg display specified by "n"
    SEG7_DisplayBarsToNthModule( barLeftDigit,
                                 barRightDigit,
                                 n);

}

void stepPatternBars(void)
{
	LEDR_WriteData(PATTERN_SPECIAL[++patternSpecialPosition%PATTERN_SPECIAL_LEN]);
	

	if (++patternBarsOffCount > 10)
	{
		uint8_t bar = 0;
		bar = PATTERN_DISPLAY_BARS[patternBarsPosition%PATTERN_BARS_LEN];
		patternBarsPosition++;
		SEG7_DisplayBarsToNthModule( bar, bar, 0);
		SEG7_DisplayBarsToNthModule( bar, bar, 1);
		SEG7_DisplayBarsToNthModule( bar, bar, 2);
		patternBarsOffCount = 0;
	}
}
