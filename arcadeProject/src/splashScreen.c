#include "../include/splashScreen.h"
#include <unistd.h>

int DrawInstructions(SplashScreen * screen);
int DrawScores(SplashScreen * screen);
int GetDifficulty(SplashScreen * screen);

const int SPLASH_SCREEN_REFRESH_PERIOD = 50000;

int InitSplashScreen(SplashScreen * screen)
{
    screen->m_highscore = 0;
    InitFeild(&screen->m_feild);
    return 0;
}

int CleanupSplashScreen(SplashScreen * screen)
{
    screen->m_highscore = 0;
    CleanupFeild(&screen->m_feild);
    return 0;
}

int RunSplashScreen(SplashScreen * screen, int oldScore)
{
    enum state_t {SCORE_SCREEN,
                  INSTRUCTIONS_SCREEN, PLAY};
    enum state_t state = SCORE_SCREEN;
    int selectedDifficulty = 1;   
    int tempDifficulty = 1;
    int rawSwitchRead = 0;
    if (oldScore > screen->m_highscore)
        screen->m_highscore = oldScore; 

    while (state != PLAY)
    {
        switch(state)
        {  
            case SCORE_SCREEN: 
                DrawScores(screen);
                if (readPushButtons() != 0x0F)
                {
                    while(readPushButtons() != 0x0F)
                    { usleep(1); }
                    state = INSTRUCTIONS_SCREEN;
                    DrawInstructions(screen);
                }
                //If sw0 goes high
                rawSwitchRead = readSwitches();
                if ((rawSwitchRead) & 0x01)
                {
                    tempDifficulty = GetDifficulty(screen);
                    //Check if difficluty is valid.
                    if ((tempDifficulty > 0) &&
                        (tempDifficulty < 10))
                    {
                        // play the game with specified
                        //Dificulty.
                        selectedDifficulty = tempDifficulty;

                        state = PLAY;
                    }

                }
                break;
            case INSTRUCTIONS_SCREEN: 
                if (readPushButtons() != 0x0F)
                {
                    while(readPushButtons() != 0x0F)
                    { usleep(1); }
                    state = SCORE_SCREEN;
                }
                break;
        }
        usleep(SPLASH_SCREEN_REFRESH_PERIOD*2);
    }


    return selectedDifficulty;
}


///////////////// \/ Private Functions  \/ /////////////////

int DrawInstructions(SplashScreen * screen)
{
    clear();
    DrawFeildBorder(&screen->m_feild);
    mvprintw(2, 16, "OBJECTIVE:");
    mvprintw(3, 2, "Steer your UFO to collect all 5 beacons.");
    mvprintw(4, 2, "line up the heart (*) of your UFO over the beacon (-5-) to collect it.");
    mvprintw(5, 2, "Colleting beacons earns you 50 points and gives you back some fuel.");
    mvprintw(6, 2, "If you crash your UFO into the walls, the game ends.");
    mvprintw(7, 16, "YOUR SHIP CONTROLS:");
    mvprintw(8, 2, "the UFO has 4 engines, each controlled by the 4 SOC buttons");
    mvprintw(10, 8, "   left thruster -> O O O O <- right thruster");
    mvprintw(11, 8, "left/down thruster---/   \\---right/down thruster");
    mvprintw(13, 2, "Each thruster provides acceleration in the opposite direction.");
    mvprintw(14, 2, "Each of these engines draw fuel from three different tanks.");
    mvprintw(15, 2, "Left engine pulls from left tank, right from right,");
    mvprintw(16, 2, "and the middle two engines both pull from the middle tank");
    mvprintw(17, 2, "Fuel levels are shown on the 7-Seg display.");
    mvprintw(18, 16, "DIFFICULTY:");    
    mvprintw(19, 2, "Higher difficulties add a multiplier to your score.");
    mvprintw(20, 2, "You also collect less fuel from each beacon.");
    mvprintw(21, 16, "Press any button to return");
    refresh();
    return 0;
}

int DrawScores(SplashScreen * screen)
{
    clear();
    DrawFeildBorder(&screen->m_feild);
    mvprintw(2, 24, "!!! Welcome To Invader Space !!!");
    mvprintw(3, 32, "High Score:");
    mvprintw(3, 48, "%d", screen->m_highscore);
    
    mvprintw(6, 16, "Press any button on the SOC board to see instructions");
    mvprintw(8, 16, "Toggle Switch 0 UP to begin dificulty selection.");
    mvprintw(9, 16, "Use Switches 9-1 to select corersponding difficulty.");
    mvprintw(10, 16, "Toggle switch 0 DOWN when completed to start game.");
    refresh();
    return 0;
}
int GetDifficulty(SplashScreen * screen)
{
    int rawSwitchRead = readSwitches();
    int readDifficulty = 0;
    while (rawSwitchRead & 0x01)     
    {
        rawSwitchRead = rawSwitchRead >> 1;
        switch(rawSwitchRead)
        {   
            case 0x001: readDifficulty = 1; break;
            case 0x002: readDifficulty = 2; break;
            case 0x004: readDifficulty = 3;break;
            case 0x008: readDifficulty = 4 ;break;
            case 0x010: readDifficulty = 5 ;break;
            case 0x020: readDifficulty = 6 ;break;
            case 0x040: readDifficulty = 7 ;break;
            case 0x080: readDifficulty = 8 ;break;
            case 0x100: readDifficulty = 9 ;break;
            default: readDifficulty = 0 ;break;
        }  

        DrawScores(screen);
        mvprintw(13,32,"Difficulty: ");
        mvprintw(13,48,"%d",readDifficulty);        
        refresh();

        usleep(SPLASH_SCREEN_REFRESH_PERIOD);

        rawSwitchRead = readSwitches();
    }
    
    return readDifficulty;
}


