#include "../include/collisionController.h"
#include <ncurses.h>

int InitCollisionController(CollisionController * controller)
{
    CleanupCollisionController(controller);
    return 0;
}

int CleanupCollisionController(CollisionController * controller)
{
    controller->m_numCollisions = 0;
    controller->m_x_diff = 0;
    controller->m_y_diff = 0;
    return 0;
}

int CheckCollisionFeild(CollisionController * controller,
                        Feild * feild,
                        Position * checkPosition)
{
    int collisionTrue = 0;
    //Is the position past the top?
    if (checkPosition->m_y <= feild->m_origin_y)
    {
        collisionTrue = 1;
    }
    //Is the position past the bottom?
    else if (checkPosition->m_y >= feild->m_max_y)
    {
         collisionTrue = 1;
    }
    //Is the position past the left?
    else if (checkPosition->m_x <= feild->m_origin_x)
    {
        collisionTrue = 1;
    }   
    //Is the position past the right?
    else if (checkPosition->m_x >= feild->m_max_x)
    {
         collisionTrue = 1;
    }

    if (collisionTrue == 1)
    {
        controller->m_numCollisions++;
        return 1;
    }
    else
        return 0;
}


int DrawCollisionController(CollisionController * controller)
{
    mvprintw( 4, 2, "%d", controller->m_x_diff);
    mvprintw( 4, 2+3, ",");
    mvprintw( 4, 2+6, "%d", controller->m_y_diff);

    return 0;
}
int CheckCollisionTwoPositions(CollisionController * controller,
                                Position positionA,
                                Position positionB)
{
    int x_diff = positionA.m_x - positionB.m_x;
    int y_diff = positionA.m_y - positionB.m_y;
       
    controller->m_x_diff = x_diff;
    controller->m_y_diff = y_diff;

    if ((y_diff == 0) && (x_diff == 0))
    {       
        controller->m_numCollisions++;
        return 1;
    }
    else 
        return 0;
}



