#include "../include/beaconController.h"
#include <ncurses.h>

int DrawActiveBeaconPosition(BeaconController * beaconController);
Position GetNewValidBeaconPosition(BeaconController * controller,
                                    Feild * feild);

int const BEACON_CONTROLLER_STARTING_NUM_BEACONS = 5;
char const BEACON_SPRITE_A[4] = "-+-";
char const BEACON_SPRITE_B[4] = "-X-";
int const BEACON_SPRITE_X_OFFSET = -1;
int const BEACON_SPRITE_Y_OFFSET = 0;
int const BEACON_COUNTER_X_OFFSET = 0;
int const BEACON_COUNTER_Y_OFFSET = 0;

int InitBeaconController(BeaconController * controller, Feild * feild)
{
    CleanupBeaconController(controller);
    controller->m_activeBeacon = GetNewValidBeaconPosition(controller,
                                                                 feild);
    return 0;
}

int CleanupBeaconController(BeaconController * beaconController)
{
    beaconController->m_activeBeacon.m_x = 46;
    beaconController->m_activeBeacon.m_y = 6;
    beaconController->m_beaconsRemaining = BEACON_CONTROLLER_STARTING_NUM_BEACONS;
    return 0;
}

int DrawBeaconController(BeaconController * beaconController)
{
    //Draw player sprite
    mvprintw(beaconController->m_activeBeacon.m_y + BEACON_SPRITE_Y_OFFSET,
             beaconController->m_activeBeacon.m_x + BEACON_SPRITE_X_OFFSET,
             BEACON_SPRITE_A);
    
    //Draw number of remaining beacons
    mvprintw(beaconController->m_activeBeacon.m_y + BEACON_COUNTER_Y_OFFSET,
             beaconController->m_activeBeacon.m_x + BEACON_COUNTER_X_OFFSET,
             "%d", beaconController->m_beaconsRemaining);

    DrawActiveBeaconPosition(beaconController);
    return 0;
}

Position GetActiveBeaconPosition(BeaconController * beaconController)
{
    return beaconController->m_activeBeacon; 
}


int PlaceNextBeaconInFeild(BeaconController * controller,
                           Feild * feild)
{   
    if(controller->m_beaconsRemaining > 0)
    {
        controller->m_activeBeacon = GetNewValidBeaconPosition(controller,
                                                               feild);
        controller->m_beaconsRemaining--;
    }
    return controller->m_beaconsRemaining;
}

////////  \/ Private Functions \/ /////////////////////////////
Position GetNewValidBeaconPosition(BeaconController * controller,
                                    Feild * feild)
{
    //Values specify distance inward from feild borders.
    //Random positions must be this far from the feild border.
    int place_min_x = 10;
    int place_min_y = 5;
    int place_max_x = 10;
    int place_max_y = 5;

    //Find the valid range of possible coordinates, according to
    //parameters set for current difficulty.    
    int valid_x_range = feild->m_max_x - place_min_x - place_max_x;
    int valid_y_range = feild->m_max_y - place_min_y - place_max_y;
    
    //Saftey Check! If the valid range is negative for some weird reason, 
    // just set it to 1
    if(valid_x_range < 0)
        valid_x_range = 1;
    if(valid_y_range < 0)
        valid_y_range = 1;    

    //Get two random, positive numbers
    int raw_random_1 = GetRandomNumber();
    int raw_random_2 = GetRandomNumber();
    if (raw_random_1 < 0)
        raw_random_1 *= -1;
    if (raw_random_2 < 0)
        raw_random_2 *= -1;

    //Get random coordinates within the valid range
    int valid_rand_x = raw_random_1%valid_x_range;
    int valid_rand_y = raw_random_2%valid_y_range;

    //Offset the random coordinates to set them within the playing feild,
    //As far from the top and bottom of the feild as the current difficulty
    //Specifies.
    int newX = valid_rand_x + place_min_x;
    int newY = valid_rand_y + place_min_y;

    //Use the cooridnates to create a position.
    Position returnThis;
    returnThis.m_x = newX;
    returnThis.m_y = newY;
    
    return returnThis;
}

int DrawActiveBeaconPosition(BeaconController * beaconController)
{
    const int DRAW_POS_AT_Y = 3;
    const int DRAW_POS_AT_X = 2;

    mvprintw(DRAW_POS_AT_Y,
             DRAW_POS_AT_X,
             "%d", beaconController->m_activeBeacon.m_x);
    
    mvprintw(DRAW_POS_AT_Y,
             DRAW_POS_AT_X+3,
             ",");

    mvprintw(DRAW_POS_AT_Y,
             DRAW_POS_AT_X+6,
             "%d", beaconController->m_activeBeacon.m_y);

    return 0;
}



