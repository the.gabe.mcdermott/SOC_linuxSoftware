#include "../include/randomCore.h"
#include "../include/hpsSystem.h"

extern volatile unsigned long *h2p_randomCore_addr;

int GetRandomNumber(void)
{
	//Garenteed random by fair dice roll
	int randomNum = 4;
	randomNum = alt_read_word(h2p_lw_randomCore_addr);
	return randomNum;
}
