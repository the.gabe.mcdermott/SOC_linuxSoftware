#include <ncurses.h>
#include <unistd.h>
#include "../include/hps_0.h"
#include "../include/hpsSystem.h"
#include "../include/randomCore.h"
#include "../include/gameEngine.h"
#include "../include/splashScreen.h"

int main(int argc, char **argv)
{
    void *virtual_base = NULL;
    int fd = 0;
    int sys_ok = 0;
    int difficulty = 1;
    int score = 0;
    mapAddresses(&virtual_base, &fd);     
    
    sleep(1);

    GameEngine game;     
    InitGame(&game);

    SplashScreen splashScreen;
    
    while (sys_ok == 0)
    {
        //run splash screen while idle
        InitSplashScreen(&splashScreen);
        difficulty = RunSplashScreen(&splashScreen, score);
        
        //If the splash screen exited normally.
        //Start the game
        InitGame(&game);
            
        //Play the game
        //Returns the score from that play
        score = RunGameLoop(&game, difficulty);
            
        //Reset game
        CleanupGame(&game);
    }
    
    //Clean up before exit.
    CleanupGame(&game);
    CleanupSplashScreen(&splashScreen);
    unmapAddresses(&virtual_base, &fd);
    
    return 0;
}
