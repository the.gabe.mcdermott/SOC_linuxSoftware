#include "../include/gameEngine.h"
#include "../include/randomCore.h"
#include "../include/pushButtons.h"
#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

#define ESCAPE_KEY 27


int DrawGame(GameEngine * game);
int DoGameLogic(GameEngine * game);
int DoCollisionLogic(GameEngine * game);

const int SCORE_BASE_INCRIMENT = 50;
const int BASE_FUEL_REWARD = 900;
const int PRINT_SCORE_POS_X = 2;
const int PRINT_SCORE_POS_Y = 5;

int InitGame(GameEngine * game)
{
    game->m_numPlays = 0;
    game->m_score = 0;
    game->m_difficulty = 1;
    InitFeild(&game->m_feild);
    InitPlayer(&game->m_player);
    InitCollisionController(&game->m_collisionController);
    InitBeaconController(&game->m_beaconController, &game->m_feild);  
    initscr();
    noecho();
    curs_set(FALSE);
    return 0;   
}

int CleanupGame(GameEngine * game)
{
    endwin();
    game->m_numPlays = 0;
    game->m_score = 0;
    game->m_difficulty = 1;
    CleanupCollisionController(&game->m_collisionController);
    CleanupBeaconController(&game->m_beaconController);
    CleanupPlayer(&game->m_player);
    CleanupFeild(&game->m_feild);
    return 0;
}

int RunGameLoop(GameEngine * game, int difficulty)
{
    char input = 0;
    int gameOver = 0;
    game->m_numPlays++;
    if (difficulty < 1)
        difficulty = 1;
    if (difficulty > 9)
        difficulty = 9;
    game->m_difficulty = difficulty;
    while (input != ESCAPE_KEY && gameOver == 0)
    {
        //randomNum = GetRandomNumber();
    
        //mvprintw(20, 20, "Wow such gameplay");
        //mvprintw(21, 20, "RandomNum: %d", randomNum);
        //mvprintw(22, 20, "Number of plays: %d", game->m_numPlays);
        //mvprintw(23, 20, "Press any key to win");
        
        gameOver = DoGameLogic(game);  
        DrawGame(game);
        usleep(300000);
        //input = getch();
    }
    clear();
    return game->m_score;
} 

int DrawGame(GameEngine * game)
{
    clear();
    DrawFeildBorder(&game->m_feild);
    DrawPlayer(&game->m_player);
    DrawBeaconController(&game->m_beaconController);
    DrawCollisionController(&game->m_collisionController);
    
    mvprintw(PRINT_SCORE_POS_Y,
             PRINT_SCORE_POS_X,
             "%d", game->m_score);


    refresh();
    return 0;
}


int DoGameLogic(GameEngine * game)
{
    int gameOver = 0;
    DoPlayerLogic(&game->m_player);
    gameOver = DoCollisionLogic(game);
    return gameOver;
}

//////////////////////////////////////
// Return 0 if game continue
// Return 1 if game over.
/////////////////////////////////////
int DoCollisionLogic(GameEngine * game)
{
    int gameOver = 0;
    Position playerPosition;

    //Check for collision between player and feild boundary
    playerPosition = GetPlayerPosition(&game->m_player);
    
    if (CheckCollisionFeild(&game->m_collisionController,
                        &game->m_feild,
                        &playerPosition) == 1)
    {
        //Player has left the feild.
        //Game Over
        gameOver = 1;
    }

    //Check if collision with active beacon
    Position beaconPosition;
    beaconPosition = GetActiveBeaconPosition(&game->m_beaconController);
    playerPosition = GetPlayerPosition(&game->m_player);

    if(CheckCollisionTwoPositions(&game->m_collisionController,
                               playerPosition,
                               beaconPosition) == 1)
    {
        //Refill Fuel according to difficulty.
        //Higher difficulty means a lower refill factor.
        PlayerAddFuelToTanks(&game->m_player, 
                             BASE_FUEL_REWARD/game->m_difficulty);
        //Increment score
        game->m_score += SCORE_BASE_INCRIMENT*game->m_difficulty;
        //Place next beacon
        if (PlaceNextBeaconInFeild(&game->m_beaconController,
                                    &game->m_feild) == 0)
            gameOver = 1;
    }
    return gameOver;
}
