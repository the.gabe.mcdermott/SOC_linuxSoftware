#include "../include/feild.h"
#include "../include/position.h"
#include <ncurses.h>

#define BORDER_TOP '-'
#define BORDER_SIDE '|'

int InitFeild(Feild * feild)
{
    int const ORIGIN_POS_X = 0;
    int const ORIGIN_POS_Y = 0;
    int const MAX_X_POS = 79;
    int const MAX_Y_POS = 23;

    feild->m_origin_x = ORIGIN_POS_X;
    feild->m_origin_y = ORIGIN_POS_Y;
    feild->m_max_x = MAX_X_POS;
    feild->m_max_y = MAX_Y_POS;
    return 0;
}


int CleanupFeild(Feild * feild)
{
    feild->m_origin_x = 0;
    feild->m_origin_y = 0;
    feild->m_max_x = 0;
    feild->m_max_y = 0;
    return 0;
}


int DrawFeildBorder(Feild * feild)
{   
    
    //Draw top
    mvhline(feild->m_origin_y, feild->m_origin_x, 
            BORDER_TOP, feild->m_max_x);
    
    //Draw Bottom
    mvhline(feild->m_max_y, feild->m_origin_x,
          BORDER_TOP, feild->m_max_x);

    //Draw Left
    mvvline(feild->m_origin_y, feild->m_origin_x,
            BORDER_SIDE, feild->m_max_y);

    //Draw right
    mvvline(feild->m_origin_y, feild->m_max_x,
            BORDER_SIDE, feild->m_max_y);
    return 0;
}

int HorDistanceFromXOrigin(Feild * feild, Position pos)
{   
    return pos.m_x - feild->m_origin_x;   
}

int HorDistanceFromXMax(Feild * feild, Position pos)
{
    return pos.m_x - feild->m_max_x;
}

int VertDistanceFromYOrigin(Feild * feild, Position pos)
{
    return pos.m_y - feild->m_origin_y;
}

int VertDistanceFromYMax(Feild * feild, Position pos)
{
    return pos.m_y - feild->m_max_y;
}

