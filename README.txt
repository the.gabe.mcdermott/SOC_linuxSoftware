Project Files for final projects for OIT's SOC Course.
This repo contains two projects:

* takeHomeFinal: 
An assignment to create a system that performed a variety of tasks depending on the selected mode.
These tasks included doing arithmatic, generating fibanocii numbers, and displaying an LED light pattern.

* arcadeProject: 
A more open ended assignment. I created a simple arcade game.
The player flies an ASCII spaceship around on an SSH terminal and tries to collect fuel beacons without hitting the walls.

These projects were buit on a DE1-SOC development board.

http://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&No=836

I wrote the modules for these projects in Verilog using Quartus, and the software in C using vi and gcc/make. 
The Quartus IDE has a tool I used to generate the bus and drivers connecting the FPGA to the boards processor.
